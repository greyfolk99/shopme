# SHOPME
- 포트폴리오용 간단 웹사이트
- 로컬 인증 회원가입 / 주문 / 상품 관리 기능이 있는 가상 쇼핑몰
- www.shopme.space (AWS 과금시 내려갈 수 있습니다...)ㅅ

### 프로젝트 구조
![shopme](https://user-images.githubusercontent.com/115934563/223373062-efce0707-bf7e-42fe-a388-a223977c349b.png)

- 프로젝트 구조는 위와 같이 진행되었습니다.
- 추가 개선 가능 사항:
  1. S3 bucket을 이용한 이미지 저장
  2. docker를 이용한 컨테이너 환경 구축
  3. 로드 밸런싱 툴 적용

### ERD
![mermaid-diagram-2023-02-26-123533](https://user-images.githubusercontent.com/115934563/223372203-18147459-c74e-425c-bae2-a67c3a423190.png)
